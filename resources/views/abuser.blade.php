@extends('app')
@section('content')
<div class="abuser-action container">
  <div class="btn-group abusers-button" role="group" aria-label="...">
    <button type="button" class="btn btn-success button-generate-abuser">Generate data</button>
  </div>
</div>
<div class="abusers container col-md-8">
</div>

<div class="form-abuser col-md-2">
  <form method accept-charset="UTF-8" id="abuserform">
    <input name="_token" type="hidden" value={{ csrf_token() }}>
    <div class="form-group">
      <label for="abusers">Select month</label>
      <select class="form-control" id="abusers">
        @foreach ($month as $key=>$item)
            <option data-mid={{$key}}>{{ $item }}</option>
        @endforeach
      </select>
    </div>
    <button type="submit" class="btn btn-default btn-info">Show report</button>
  </form>
</div>
@stop
