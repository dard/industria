@extends('app')
@section('content')
  <div class="add-user container">
    <div class="btn-group users-button" role="group" aria-label="...">
      <button type="button" class="btn btn-success button-add-user">add</button>
      <button type="button" class="btn btn-primary button-get-user">get users</button>
    </div>
  </div>
  <div class="users container col-md-8">
  </div>
  <div class="form-user col-md-4">
    <form method accept-charset="UTF-8" id="userform">
      <input name="_token" type="hidden" value={{ csrf_token() }}>
      <div class="form-group">
        <label for="name">name</label>
        <input type="text" class="form-control" id="name">
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" id="email">
      </div>
      <div class="form-group">
        <label for="companies">Select user's company</label>
        <select class="form-control" id="companies"></select>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
@stop
