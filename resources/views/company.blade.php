@extends('app')
@section('content')
  <div class="add-company container">
    <div class="btn-group companies-button" role="group" aria-label="...">
      <button type="button" class="btn btn-success button-add-company">add</button>
      <button type="button" class="btn btn-primary button-get-company">get companies</button>
    </div>
  </div>
  <div class="companies container col-md-8">
  </div>
  <div class="form-company col-md-4">
    <form method accept-charset="UTF-8" id="companyform">
      <input name="_token" type="hidden" value={{ csrf_token() }}>
      <div class="form-group">
        <label for="name">name</label>
        <input type="text" class="form-control" id="name">
      </div>
      <div class="form-group">
        <label for="quota">Data size in TB</label>
        <input type="text" class="form-control" id="quota">
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form>
  </div>
@stop
