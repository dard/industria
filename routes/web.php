<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('user');
});

Route::get('user/all','UserController@getAll');
Route::resource('/user','UserController');

Route::get('company/all','CompanyController@getAll');
Route::resource('/company','CompanyController');

Route::get('abuser/all/{id}','AbuserController@getAll');
Route::resource('/abuser','AbuserController');
