$(function() {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="token"]').attr('value')
    },
    error : function(jqXHR, textStatus, errorThrown) {
      $('.error-box').remove();
      $('body').prepend(buildHTML.buildErrorBox(jqXHR));
    },
  });

  var buildHTML = new function(){
    this.buildErrorBox =  function(jqXHR){
      var res = `<div class='container error-box'>`;
      for (var i in jqXHR.responseJSON) {
        if (jqXHR.responseJSON.hasOwnProperty(i)) {
          var obj = jqXHR.responseJSON[i];
          for (var variable in obj) {
            if (obj.hasOwnProperty(variable)) {
              res +='<p>'+obj[variable]+'</p>';
              // $('body').find('input#'+i).css({"border-color" : 'red'});
            }
          }
        }
      }
      res += '</div>';
      return res;
    };

    this.addUserRow =  function(obj){
      var res = ` <div class="username container" data-uid=`+obj.id+` data-cid=`+obj.company_id+`>
        <span class="col-md-3 name">`+obj.name+`</span>
        <span class="col-md-2 email">`+obj.email+`</span>
        <span class="col-md-1 company">`+obj.company_name+`</span>
        <div class="btn-group col-md-2" role="group" aria-label="...">
        <button type="button" class="btn btn-default button-edit-user">edit</button>
        <button type="button" class="btn btn-default button-delete-user">delete</button></div></div>`;
      return res;
    };
    this.addCompanyRow = function(obj){
      var res = ` <div class="company container" data-cid=`+obj.id+`>
        <span class="col-md-4 name">`+obj.name+`</span>
        <span class="quota">`+obj.quota/1000000000000+`</span>
        <span>&nbsp;TB</span>
        <div class="btn-group col-md-2" role="group" aria-label="...">
        <button type="button" class="btn btn-default button-edit-company">edit</button>
        <button type="button" class="btn btn-default button-delete-company">delete</button></div></div>`;
      return res;
    }
    this.addAbuserRow = function(obj){
      var res = ` <div class="abuser container" data-aid=`+obj.id+`>
        <span class="col-md-2 name">`+obj.name+` </span>
        <span class="quota">`+obj.quota/1000000000000+` TB</span>
        <span class="bytes-used col-md-2">`+(obj.bytes/1000000000000).toFixed(2)+` TB</span></div>`;
      return res;
    }

    this.addCompanySelect = function(data){
      var res = '';
      $.each(data, function( i, value ) {
        res += `<option value=`+data[i].id+`>`+data[i].name+`</option>`;
      });
      return res;
    }
  };

  var serverInfo = new function(){
    this.getCompanies =  function(){
      return $.ajax({
        method: "GET",
        url: "company/all",
        //dataType: "json",
      });
    };
  };

  $('form').submit(function() {
      return false;
  });

// USER START
  $('form#userform').submit(function() {
    var name = $('#userform #name').val();
    var email = $('#userform #email').val();
    var companyId = $("#userform #companies").val();
    var companyName = $("#userform #companies option:selected").text();
    var method = $('#userform').attr('method');
    var url = $('#userform').attr('url');
    $.ajax({
      url: url,
      method: "POST",
      dataType: "json",
      data: {name : name, email : email, _method: method, company_id : companyId}
    }).done(function(data){
      $("form#userform").trigger("reset");
      if(data.updated === true){
        var cont = $('.username.container[data-uid='+data.id+']');
        $('.username.container[data-uid='+data.id+']').attr('data-cid', companyId);
        $(cont).find('span.name').text(name);
        $(cont).find('span.email').text(email);
        $(cont).find('span.company').text(companyName);
      } else if(data.created === true){
        var res = buildHTML.addUserRow({
          id : data.info.id,
          name : data.info.name,
          email : data.info.email,
          company_name : data.info.company_name,
          company_id : data.info.company_id,
        });
        $(".users.container").append(res);
      }
    });
  });

  $('.button-add-user').click(function(){
    $("#userform").trigger("reset");
    $('#userform').attr('method', 'POST');
    $('#userform').attr('url', 'user');
  });

  $(document).on('click', '.button-edit-user, .button-add-user', function() {
    if($('#companies').text() !== ''){
      return;
    }
    var that = this;
    var data = serverInfo.getCompanies();
    data.done(function(data){
      var selectHTML = buildHTML.addCompanySelect(data);
      $("#companies").append(selectHTML);
      var companyId = $(that).closest('.username.container').attr('data-cid');
      $('#companies').val(companyId);
      $('.form-user').show();
    });
  });

  $(document).on('click', '.button-edit-user', function() {
    var id = $(this).closest('.username.container').attr('data-uid');
    var name = $(this).closest('.username.container').find('span.name').text();
    var email = $(this).closest('.username.container').find('span.email').text();
    var form = $('#userform');
    var companyId = $(this).closest('.username.container').attr('data-cid');
    $('#companies').val(companyId);
    $(form).attr({'method' : 'PUT', 'url': 'user/'+id});
    $(form).find('input#name').val(name);
    $(form).find('input#email').val(email);
  });

  $('.button-get-user').click(function() {
    $.ajax({
      method: "GET",
      url: "user/all",
      //dataType: "json",
    }).done(function(data){
      $(".users.container").empty().show();
      $.each(data, function( i, value ) {
        var res = buildHTML.addUserRow({
          id : data[i].id,
          name : data[i].name,
          email : data[i].email,
          company_name : data[i].company_name,
          company_id : data[i].company_id,
        });
        $(".users.container").append(res);
      });
    });
  });

  $(document).on('click', '.button-delete-user', function(){
    var cont = (this).closest('.username.container');
    var id = $(cont).data('uid');
    $.ajax({
      method: "POST",
      url: "user/"+id,
      dataType: "json",
      data: {_method: 'delete'}
    }).done(function(data){
      if(data.status === 1){
        $(cont).remove();
      }
    });

  });
// END USER

// COMPANY START
  $('form#companyform').submit(function() {
    var name = $('#companyform #name').val();
    var quota = $('#companyform #quota').val();
    var quotaTB = quota * 1000000000000;
    var method = $('#companyform').attr('method');
    var url = $('#companyform').attr('url');
    $.ajax({
      url: url,
      method: "POST",
      dataType: "json",
      data: {name : name, quota : quotaTB, _method: method}
    }).done(function(data){
      $("form#companyform").trigger("reset");
      if(data.updated === true){
        var cont = $('.company.container[data-cid='+data.id+']');
        $(cont).find('span.name').text(name);
        $(cont).find('span.quota').text(quota);
      } else if(data.created === true){
        var res = buildHTML.addCompanyRow({
          id : data.info.id,
          name : data.info.name,
          quota : data.info.quota
        });
        $(".companies.container").append(res);
      }
    });
  });

  $('.button-get-company').click(function() {
    var data = serverInfo.getCompanies();
    data.done(function(data){
      $(".companies.container").empty().show();
      $.each(data, function( i, value ) {
        var res = buildHTML.addCompanyRow({
          id : data[i].id,
          name : data[i].name,
          quota : data[i].quota
        });
        $(".companies.container").append(res);
      });
    });
  });

  $('.button-add-company').click(function(){
    $("#companyform").trigger("reset");
    $('#companyform').attr('method', 'POST');
    $('#companyform').attr('url', 'company');
    $('.form-company').show();
  });

  $(document).on('click', '.button-edit-company', function() {
    var id = $(this).closest('.company.container').data('cid');
    var name =$(this).closest('.company.container').find('span.name').text();
    var quota =$(this).closest('.company.container').find('span.quota').text();
    var form = $('#companyform');
    $(form).attr({'method' : 'PUT', 'url': 'company/'+id});
    $(form).find('input#name').val(name);
    $(form).find('input#quota').val(quota);
    $('.form-company').show();
  });

  $(document).on('click', '.button-delete-company', function(){
    var cont = (this).closest('.company.container');
    var id = $(cont).data('cid');
    $.ajax({
      method: "POST",
      url: "company/"+id,
      dataType: "json",
      data: {_method: 'delete'}
    }).done(function(data){
      if(data.status === 1){
        $(cont).remove();
      }
    });
  });
// COMPANY END

// ABUSER START
$('form#abuserform').submit(function() {
  var monthId = parseInt($("#abuserform #abusers option:selected").attr('data-mid')) + 1;
  $.ajax({
    url: 'abuser/all/'+monthId,
    method: "GET",
    dataType: "json",
  }).done(function(data){
    $(".abusers.container").empty().append(`<div class="abuser container" data-aid="1">
                                    <span class="col-md-2 name">Company name </span>
                                    <span class="quota">Quota</span>
                                    <span class="bytes-used col-md-2">Used</span>
                                    </div>`);
    $.each(data, function( i, value ) {
      $(".abusers.container").append(buildHTML.addAbuserRow(data[i])).show();
    });
  });
});

$('.button-generate-abuser').click(function(){
  $.ajax({
    url: 'abuser',
    method: "POST",
    dataType: "json",
  }).done(function(data){
    if(data.status === true){
      $(".abusers.container").empty().append(`<span class='sucess'>Mock data has been generated</span>`).show();
    }
  });
  $('.abusers.container').empty().show();
});

// ABUSER END
});
