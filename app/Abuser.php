<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;

class Abuser extends Model
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'user_id', 'count_of_bytes'
  ];

  public $month = [ 'January', 'February', 'March', 'April',
                    'May', 'June', 'July', 'August',
                    'September', 'October', 'November', 'December'];
  public $toDB = [];

  public function getLastSixMonth(){
    $monthNum =  Carbon::now()->month;
    $numInArray = $monthNum-1;
    $next = array_slice($this->month, $monthNum ,6, true);
    $diff = array_diff($this->month, $next);
    unset( $diff[$numInArray]);
    $diff[$numInArray] = $this->month[$numInArray];
    return $diff;
  }

  public function setMockData(){
    $element = 1;
    $allUsers = DB::table('users')->select('id as user_id', 'name',
                                  'email', 'company_id')->get();
    $halfYearAgo = Carbon::Now()
                  ->setTimezone('Europe/Kiev')
                  ->addHour(2)->subMonth(5)->timestamp;

    foreach ($allUsers as $key => $value) {
      $bytesSize= 0;
      $opperationCount = rand(50, 500);
      //one row per month+
      for ($k=1; $k <= 6; $k++, $element++, $opperationCount--) {
        $bytesSize += $this->BigRandomNumber(100, 10000000000000);
        $this->toDB[$element]['user_id'] = $value->user_id;
        $this->toDB[$element]['count_of_bytes'] = $bytesSize;
        $this->toDB[$element]['created_at'] = Carbon::createFromTimestamp($halfYearAgo)
                                    ->addMonth($k);
      }

      for ($i = 1; $i <= $opperationCount; $i++, $element++) {
        $bytesSize += $this->BigRandomNumber(100, 10000000000000);
        $this->toDB[$element]['user_id'] = $value->user_id;
        $this->toDB[$element]['count_of_bytes'] = $bytesSize;
        $this->toDB[$element]['created_at'] = Carbon::createFromTimestamp($halfYearAgo)
                                    ->addMonth(rand(0,5))->addHour(rand(0,24))
                                    ->addMinute(rand(0,60));
      }
    }
    $res = DB::table('operations')->insert($this->toDB);
    return ['status'=>$res];
  }

  public function getAbusers($id){
    $now = Carbon::Now()->setTimezone('Europe/Kiev');
    $month = $now->month;
    if($id>$month){
      $mToday= Carbon::create($now->subYear()->year, $id, 1, 0);
      $mNext = Carbon::create($now->year, $id, 1, 0)->addMonth();
    } else {
      $mToday = Carbon::create($now->year, $id, 1, 0);
      $mNext = Carbon::create($mToday->year, $id, 1, 0)->subMonth();
    }
    if($mToday->year > $mNext->year){
      $between = [$mNext->toDateTimeString(), $mToday->toDateTimeString()];
    } else {
      $between = [$mToday->toDateTimeString(), $mNext->toDateTimeString()];
    }
    return $this->getAbusersByTime($between);
  }

  public function getAbusersByTime($time){
    return DB::table('users')
    ->join('companies', 'users.company_id', '=', 'companies.id')
    ->join('operations', 'users.id', '=', 'operations.user_id')
    ->select('companies.name', 'companies.id', 'companies.quota',
              DB::raw('SUM(operations.count_of_bytes) as bytes'))
    ->whereBetween('operations.created_at', $time)
    ->havingRaw('SUM(operations.count_of_bytes) > companies.quota')
    ->groupBy('companies.name', 'companies.id', 'companies.quota')
    ->get();
  }

  public function BigRandomNumber($min, $max) {
    $difference   = bcadd(bcsub($max,$min),1);
    $rand_percent = bcdiv(mt_rand(), mt_getrandmax(), 8);
    return (int) bcadd($min, bcmul($difference, $rand_percent, 8), 0);
  }
}
