<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('operations', function (Blueprint $table) {
         $table->increments('id');
         $table->integer('user_id');
         $table->bigInteger('count_of_bytes');
         $table->timestamp('created_at')->nullable();
         $table->index('created_at');
       });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::drop('operations');
     }
}
