<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        [
          'name' => 'Valeriy Evdokimov',
          'email' => 'valera22@gmail.com',
          'company_id' => 1,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ],
        [
          'name' => 'Kirill Povenko',
          'email' => 'kirill1888@gmail.com',
          'company_id' => 2,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]
      ]);
    }
}
