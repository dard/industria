<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('companies')->insert([
        [
          'name' => 'Kotletka',
          'quota' => 10000000000000,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ],
        [
          'name' => 'Pureshka',
          'quota' => 20000000000000,
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]
      ]);
    }
}
